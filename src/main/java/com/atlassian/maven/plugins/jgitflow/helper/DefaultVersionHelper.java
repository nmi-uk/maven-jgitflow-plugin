/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.maven.plugins.jgitflow.helper;

import com.atlassian.maven.plugins.jgitflow.ReleaseContext;
import com.atlassian.maven.plugins.jgitflow.exception.JGitFlowReleaseException;
import com.atlassian.maven.plugins.jgitflow.exception.ProjectRewriteException;
import static com.atlassian.maven.plugins.jgitflow.rewrite.ArtifactReleaseVersionChange.artifactReleaseVersionChange;
import static com.atlassian.maven.plugins.jgitflow.rewrite.ParentReleaseVersionChange.parentReleaseVersionChange;
import com.atlassian.maven.plugins.jgitflow.rewrite.ProjectChangeset;
import static com.atlassian.maven.plugins.jgitflow.rewrite.ProjectReleaseVersionChange.projectReleaseVersionChange;
import com.atlassian.maven.plugins.jgitflow.rewrite.ProjectRewriter;
import static com.atlassian.maven.plugins.jgitflow.rewrite.ScmDefaultHeadTagChange.scmDefaultHeadTagChange;
import java.util.List;
import java.util.Map;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.logging.AbstractLogEnabled;

/**
 * A helper class which updates all the poms in a project with a new version
 * 
 * @author Tamsin Slinn <tamsin@nmi.uk.com>
 */
public class DefaultVersionHelper extends AbstractLogEnabled implements VersionHelper {
    
    private ProjectRewriter projectRewriter;
    
    @Override
    public final void updatePomsWithNewVersion(ReleaseContext ctx, List<MavenProject> reactorProjects, Map<String, String> originalVersions, Map<String, String> developmentVersions) throws JGitFlowReleaseException
    {
        
        getLogger().info("updating poms for all projects...");
        
        if(!getLogger().isDebugEnabled())
        {
            getLogger().info("turn on debug logging with -X to see exact changes");
        }
        for (MavenProject project : reactorProjects)
        {
            ProjectChangeset changes = new ProjectChangeset()
                    .with(parentReleaseVersionChange(originalVersions, developmentVersions))
                    .with(projectReleaseVersionChange(developmentVersions))
                    .with(artifactReleaseVersionChange(originalVersions, developmentVersions, ctx.isUpdateDependencies()))
                    .with(scmDefaultHeadTagChange(developmentVersions));
            try
            {
                getLogger().info("updating pom for " + project.getName() + "...");

                projectRewriter.applyChanges(project, changes);

                logChanges(changes);
            }
            catch (ProjectRewriteException e)
            {
                throw new JGitFlowReleaseException("Error updating poms with next versions", e);
            }
        }
    }
    
    private void logChanges(ProjectChangeset changes)
    {
        if(getLogger().isDebugEnabled())
        {
            for (String desc : changes.getChangeDescriptionsOrSummaries())
            {
                getLogger().debug("  " + desc);
            }
        }
    }

}
