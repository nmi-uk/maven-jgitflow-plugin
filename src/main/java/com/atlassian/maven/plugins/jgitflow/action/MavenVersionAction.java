/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.maven.plugins.jgitflow.action;

import com.atlassian.jgitflow.core.ExternalAction;
import com.atlassian.jgitflow.core.JGitFlow;
import com.atlassian.jgitflow.core.exception.ExternalActionFailedException;
import com.atlassian.maven.plugins.jgitflow.ReleaseContext;
import com.atlassian.maven.plugins.jgitflow.exception.JGitFlowReleaseException;
import com.atlassian.maven.plugins.jgitflow.helper.ProjectHelper;
import com.atlassian.maven.plugins.jgitflow.helper.VersionHelper;
import java.util.List;
import java.util.Map;
import org.apache.maven.project.MavenProject;
import org.eclipse.jgit.api.errors.GitAPIException;

/**
 * This updates the version on the current branch.
 * 
 * It is for use in the release-finish process, so that the release branch can be merged with the develop branch
 * 
 * @author Tamsin Slinn <tamsin@nmi.uk.com>
 */
public final class MavenVersionAction implements ExternalAction {
    
    private final JGitFlow flow;

    private final ReleaseContext ctx;
    private final ProjectHelper projectHelper;
    private final VersionHelper versionHelper;
    
    private final String releaseLabel;

    private final List<MavenProject> releaseProjects;
    private final Map<String, String> originalVersions;
    private final Map<String, String> developmentVersions;

    /**
     * Update version action
     * 
     * @param flow
     * @param ctx
     * @param projectHelper
     * @param versionHelper
     * @param developProjects
     * @param originalVersions
     * @param developmentVersions 
     */
    public MavenVersionAction(JGitFlow flow, ReleaseContext ctx, ProjectHelper projectHelper, VersionHelper versionHelper, List<MavenProject> releaseProjects, Map<String, String> originalVersions, Map<String, String> developmentVersions, String releaseLabel) {
        this.flow = flow;
        this.ctx = ctx;
        this.projectHelper = projectHelper;
        this.versionHelper = versionHelper;
        this.releaseProjects = releaseProjects;
        this.originalVersions = originalVersions;
        this.developmentVersions = developmentVersions;
        this.releaseLabel = releaseLabel;
    }
    
    @Override
    public void call() throws ExternalActionFailedException {

        
        try {
            flow.git().checkout().setName(flow.getReleaseBranchPrefix() + releaseLabel).call();
            
            //ensure we are on release branch
            versionHelper.updatePomsWithNewVersion(ctx, releaseProjects, originalVersions, developmentVersions);
            projectHelper.commitAllChanges(flow.git(), "Updating release poms to next match develop version.");            
        } catch (JGitFlowReleaseException ex) {
            throw new ExternalActionFailedException("Failed to commit updated version to release branch",ex);
        } catch (GitAPIException ex) {
            throw new ExternalActionFailedException("Unable to checkout release branch",ex);
        }
    }
    
    
}
