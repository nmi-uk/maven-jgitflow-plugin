/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.maven.plugins.jgitflow.helper;

import com.atlassian.maven.plugins.jgitflow.ReleaseContext;
import com.atlassian.maven.plugins.jgitflow.exception.JGitFlowReleaseException;
import java.util.List;
import java.util.Map;
import org.apache.maven.project.MavenProject;

/**
 * A helper class which updates all the poms in a project with a new version
 * 
 * @author Tamsin Slinn <tamsin@nmi.uk.com>
 */
public interface VersionHelper {
    
    /**
     * Update the poms with a new version
     * 
     * @param ctx
     * @param reactorProjects
     * @param originalVersions
     * @param developmentVersions
     * @throws JGitFlowReleaseException 
     */
    void updatePomsWithNewVersion(ReleaseContext ctx, List<MavenProject> reactorProjects, Map<String, String> originalVersions, Map<String, String> developmentVersions) throws JGitFlowReleaseException;
    
}
